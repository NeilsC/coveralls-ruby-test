require 'rails_helper'

RSpec.describe WelcomeController do
  it 'should get index' do
    get :index # welcome_index_url
    expect(response).to have_http_status(:ok)
    expect(response).to render_template(:index)
  end
end
